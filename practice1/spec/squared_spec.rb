require 'squared'

describe 'Everything is an object, including numbers' do
  it 'should 9.squared should be 81' do
    9.squared.should be == 81
  end
  it 'should 16.squared should be 256' do
    16.squared.should be == 256
  end
end
