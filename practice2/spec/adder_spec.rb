require "adder"

describe "Metaprogramming" do
	describe "Dynamically adding method 'plusnum' to class 'Adder'" do
		it "should return 20 when calling Adder.new(10).plus10" do
			Adder.new(10).plus10.should be == 20
		end
		it "should return 30 when calling Adder.new(10).plus20" do
			Adder.new(10).plus20.should be == 30
		end
		it "should raise an exception when calling Adder.new(10).minus20" do
			expect{ Adder.new(10).minus20 }.to raise_error
		end
	end
end