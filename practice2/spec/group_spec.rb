require "group"

describe "Extending a module" do
	describe "Method each_group_by_first_letter" do
		it "should yield ['a', ['abcd', 'axyz', 'able']], ['q', ['qrst']], ['x',['xyzab']]" do
			expect {|b| ['abcd', 'axyz', 'able', 'xyzab', 'qrst'].each_group_by_first_letter(&b) }.to yield_successive_args(['a',['abcd','axyz','able']],['q',['qrst']],['x',['xyzab']])
		end
	end
end
