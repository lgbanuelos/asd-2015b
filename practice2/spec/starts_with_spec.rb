require 'starts_with'

describe "Defining an iterator" do
	describe "each_starts_with" do
		it "should yield 'abcd' and 'able' when called with ['abcd', 'axyz', 'able', 'xyzab', 'qrst'] and 'ab'" do
			expect { |b| each_starts_with(['abcd', 'axyz', 'able', 'xyzab', 'qrst'], 'ab', &b)  }.to yield_successive_args('abcd', 'able')
		end
	end
end